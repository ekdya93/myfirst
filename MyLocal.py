# -*- coding: utf-8 -*-

import re
import urllib.request

from bs4 import BeautifulSoup
import requests
from flask import Flask
from slack import WebClient
from slackeventsapi import SlackEventAdapter
from slack.web.classes import extract_json
from slack.web.classes.blocks import *


SLACK_TOKEN = 'xoxb-689643486144-689659715957-XPTbX4EIsOGeO0RTSuf7AYjO'

SLACK_SIGNING_SECRET = 'dec56215edcb0fb1026aab4dc0f56bb9'


app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)


# 크롤링 함수 구현하기
def _crawl_portal_keywords(text):

    lang_split = text.split(':')
    n = len(lang_split)
    key_lang = lang_split[n-1].strip()
    lang_dict = {"갈리시아어" : 'gl',
"구자라트어" : 'gu',
"그리스어" : 'el',
"네덜란드어" : 'nl',
"네팔어" : 'ne',
"노르웨이어" : 'no',
"덴마크어" : 'da',
"독일어" : 'de',
"라오어" : 'lo',
"라트비아어" : 'lv',
"라틴어" : 'la',
"러시아어" : 'ru',
"루마니아어" : 'ro',
"룩셈부르크어" : 'lb',
"리투아니아어" : 'lt',
"마라티어" : 'mr',
"마오리어" : 'mi',
"마케도니아어" : 'mk',
"말라가시어" : 'mg',
"말라얄람어" : 'ml',
"말레이어" : 'ms',
"몰타어" : 'mt',
"몽골어" : 'mn',
"몽어" : 'hm',
"미얀마어 (버마어)" : 'my',
"바스크어" : 'eu',
"베트남어" : 'vi',
"벨라루스어" : 'be',
"벵골어" : 'bn',
"보스니아어" : 'bs',
"불가리아어" : 'bg',
"사모아어" : 'sm',
"세르비아어" : 'sr',
"세부아노" : 'ceb',
"세소토어" : 'st',
"소말리아어" : 'so',
"쇼나어" : 'sn',
"순다어" : 'su',
"스와힐리어" : 'sw',
"스웨덴어" : 'sv',
"스코틀랜드 게일어" : 'gd',
"스페인어" : 'es',
"슬로바키아어" : 'sk',
"슬로베니아어" : 'sl',
"신디어" : 'sd',
"신할라어" : 'si',
"아랍어" : 'ar',
"아르메니아어" : 'hy',
"아이슬란드어" : 'is',
"아이티 크리올어" : 'ht',
"아일랜드어" : 'ga',
"아제르바이잔어" : 'az',
"아프리칸스어" : 'af',
"알바니아어" : 'sq',
"암하라어" : 'am',
"에스토니아어" : 'et',
"에스페란토어" : 'eo',
"영어" : 'en',
"요루바어" : 'yo',
"우르두어" : 'ur',
"우즈베크어" : 'uz',
"우크라이나어" : 'uk',
"웨일즈어" : 'cy',
"이그보어" : 'ig',
"이디시어" : 'yi',
"이탈리아어" : 'it',
"인도네시아어" : 'id',
"일본어" : 'ja',
"자바어" : 'jw',
"조지아어" : 'ka',
"줄루어" : 'zu',
"중국어(간체)" : 'zh-CN',
"중국어(번체)" : 'zh-TW',
"체와어" : 'ny',
"체코어" : 'cs',
"카자흐어" : 'kk',
"카탈로니아어" : 'ca',
"칸나다어" : 'kn',
"코르시카어" : 'co',
"코사어" : 'xh',
"쿠르드어" : 'ku',
"크로아티아어" : 'hr',
"크메르어" : 'km',
"키르기스어" : 'ky',
"타갈로그어" : 'tl',
"타밀어" : 'ta',
"타지크어" : 'tg',
"태국어" : 'th',
"터키어" : 'tr',
"텔루구어" : 'te',
"파슈토어" : 'ps',
"펀자브어" : 'pa',
"페르시아어" : 'fa',
"포르투갈어" : 'pt',
"폴란드어" : 'pl',
"프랑스어" : 'fr',
"프리지아어" : 'fy',
"핀란드어" : 'fi',
"하와이어" : 'haw',
"하우사어" : 'ha',
"한국어" : 'ko',
"헝가리어" : 'hu',
"히브리어" : 'iw',
"힌디어" : 'hi'}

    clist = ["갈리시아어",
             "구자라트어",
             "그리스어",
             "네덜란드어",
             "네팔어",
             "노르웨이어",
             "덴마크어",
             "독일어",
             "라오어",
             "라틴어",
             "러시아어",
             "루마니아어",
             "룩셈부르크어",
             "리투아니아어",
             "마라티어",
             "마오리어",
             "마케도니아어",
             "말라가시어",
             "말라얄람어",
             "말레이어",
             "몰타어",
             "몽골어",
             "몽어",
             "미얀마어 (버마어)",
             "바스크어",
             "베트남어",
             "벨라루스어",
             "벵골어",
             "보스니아어",
             "불가리아어",
             "사모아어",
             "세르비아어",
             "세부아노",
             "세소토어",
             "소말리아어",
             "쇼나어",
             "순다어",
             "스와힐리어",
             "스웨덴어",
             "스코틀랜드 게일어",
             "스페인어",
             "슬로바키아어",
             "슬로베니아어",
             "신디어",
             "신할라어",
             "아랍어",
             "아르메니아어",
             "아이슬란드어",
             "아이티 크리올어",
             "아일랜드어",
             "아제르바이잔어",
             "아프리칸스어",
             "알바니아어",
             "암하라어",
             "에스토니아어",
             "에스페란토어",
             "영어",
             "요루바어",
             "우르두어",
             "우즈베크어",
             "우크라이나어",
             "웨일즈어",
             "이그보어",
             "이디시어",
             "이탈리아어",
             "인도네시아어",
             "일본어",
             "자바어",
             "조지아어",
             "줄루어",
             "중국어(간체)",
             "중국어(번체)",
             "체와어",
             "체코어",
             "카자흐어",
             "카탈로니아어",
             "칸나다어",
             "코르시카어",
             "코사어",
             "쿠르드어",
             "크로아티아어",
             "크메르어",
             "키르기스어",
             "타갈로그어",
             "타밀어",
             "타지크어",
             "태국어",
             "터키어",
             "텔루구어",
             "파슈토어",
             "펀자브어",
             "페르시아어",
             "포르투갈어",
             "폴란드어",
             "프랑스어",
             "프리지아어",
             "핀란드어",
             "하와이어",
             "하우사어",
             "한국어",
             "헝가리어",
             "히브리어",
             "힌디어"]

    lang_list = []
    if '@@목록' in text:
        for cdata in clist:
            lang_list.append(cdata)
        return u'\n'.join(lang_list)


    if '##' in text:

        url = 'https://www.google.com/search?source=hp&q='

        url = url + urllib.parse.quote_plus(text[15:])

        headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36'}
        source_code = requests.get(url, headers=headers).text
        soup = BeautifulSoup(source_code, "html.parser")
        titles = []
        for title in soup.find('div',class_='srg').find_all('h3',class_='LC20lb'):
            titles.append(title.get_text() + "\n")
        links = []
        for link in soup.find('div',class_='r').find_all('a'):
            links.append(link.get('href')+"\n")

        # final_text = []
        # for i in range(0,len(titles)):
        #     final_text.append(titles[i] + "\n" +links[i])

        return u''.join(titles + links)
        # return u'\n'.join(final_text)

    if '**' in text:
        url = 'https://stock.adobe.com/kr/search?load_type=search&native_visual_search=&similar_content_id=&is_recent_search=&search_type=usertyped&k=' + urllib.parse.quote_plus(text[15:])


        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36'}
        source_code = requests.get(url, headers=headers).text
        soup = BeautifulSoup(source_code, "html.parser")
        img_url = []
        for src in soup.find("a",class_="js-search-result-thumbnail non-js-link").find_all("img"):
            img_url.append(src.get('src'))



        return u''.join(img_url[0])


        # return u'\n'.join(titles)
        # return url

        # # for data in text[3:]:
    #         # #     url = url + urllib.parse.quote_plus(data) + "%20"
    #         #
    #         # headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36'}
    #         # source_code = requests.get(url, headers=headers).text
    #         # soup = BeautifulSoup(source_code, "html.parser")
    #         #
    #         # # 여기에 함수를 구현해봅시다.
    #         # transed_text = []
    #         # titles = soup.select('h3.r')
    #         # for title in titles:
    #         #     transed_text.append(title.text)
    #         #
    #         # # return u'\n'.join(transed_text)
    #         # # return u'\n'.join(transed_text)
    #         #
    #         # return print(url)


    if not key_lang in lang_dict:
        return 'tip1)"[번역문장] : [언어목록]" 형태로 작성해주세요'+'\n'+'           ex) 안녕하세요 : 그리스어'+'\n'+' tip2) [언어목록]을 보려면 "@@목록" 을 입력해주세요'+'\n'+'tip3) 구글 검색결과를 보려면 [##검색어] 형태로 입력하세요 '



    url = 'https://translate.google.co.kr/?hl=ko&tab=TT#view=home&op=translate&sl=auto&tl=' + lang_dict[key_lang] + '&text='
    keywords = lang_split[0:len(lang_split) - 1]
    mtext = ' '
    for i in range(0, len(keywords)):
        mtext = mtext + keywords[i]

    s_key = mtext.split()

    for data in s_key[1:]:
        url = url + urllib.parse.quote_plus(data) + "%20"


    print(url)
    # 여기에 함수를 구현해봅시다.
    # transed_text = []
    # for data in soup.find_all("span" , class_="translating-placeholder placeholder"):
    #     transed_text.append(data.get_text())

    return url
    # return u'\n'.join(transed_text)


# 챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]

    keywords = _crawl_portal_keywords(text)
    slack_web_client.chat_postMessage(
        channel=channel,
        text=keywords
    )


# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready.</h1>"


if __name__ == '__main__':
    app.run('127.0.0.1', port=5000)
